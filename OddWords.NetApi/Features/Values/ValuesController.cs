﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using OddWords.NetApi.Startup.Configuration;

namespace OddWords.NetApi.Features.Values
{
    [Route("api/[controller]")]
    public class ValuesController : Controller
    {
        private readonly ConnectionStrings _string;

        public ValuesController(ConnectionStrings _string)
        {
            this._string = _string;
        }

        // GET: api/values
        [HttpGet]
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return _string.DefaultConnection;
        }

        // POST api/values
        [HttpPost]
        [ValidateAntiForgeryToken]
        public void Post([FromBody]string value)
        {
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        [ValidateAntiForgeryToken]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        [ValidateAntiForgeryToken]
        public void Delete(int id)
        {
        }
    }
}
