﻿using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Logging;
using OddWords.NetApi.Startup.Configuration;
using Autofac;
using Autofac.Extensions.DependencyInjection;
using System;

namespace OddWords.NetApi.Startup
{
    public class StartupDevelopment
    {
        public IConfigurationRoot Configuration { get; }
        public IHostingEnvironment Environment { get; }
        public IContainer AutofacContainer { get; private set; }

        public StartupDevelopment(IHostingEnvironment environment)
        {
            Environment = environment;
            Configuration = CreateDefaultConfigurationRoot();
        }


        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            services.AddOptions();
            services.AddLogging();
            services.AddRouting();
            services.Configure<ConnectionStrings>(Configuration.GetSection(nameof(ConnectionStrings)));
            services.AddCors(CorsConfig.Create());
            services.AddMvc()
                    // this will automatically register controller types into the IServiceCollection when you call builder.Populate(services)
                    // because default DI only registers controller parameters not controllers itself
                    .AddControllersAsServices(); 

            var builder = new ContainerBuilder();
            builder.RegisterModule<AutofacModule>();
            builder.Populate(services);
            AutofacContainer = builder.Build();

            return new AutofacServiceProvider(AutofacContainer);
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory, IApplicationLifetime appLifetime)
        {
            loggerFactory.AddConsole();
            app.UseDeveloperExceptionPage();
            app.UseCors("AllowAllPolicy");
            app.UseWelcomePage("/");
            app.UseMvc();

            appLifetime.ApplicationStopped.Register(new Action(AutofacContainer.Dispose));
        }

        private IConfigurationRoot CreateDefaultConfigurationRoot()
        {
            return new ConfigurationBuilder()
                    .SetBasePath(Environment.ContentRootPath)
                    .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                    .AddJsonFile($"appsettings.{Environment.EnvironmentName}.json", optional: true)
                    .AddUserSecrets()
                    .AddEnvironmentVariables()
                    .Build();
        }
    }
}
