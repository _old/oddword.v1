﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Autofac;
using Microsoft.Extensions.Options;

namespace OddWords.NetApi.Startup.Configuration
{
    public class AutofacModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            //services.AddScoped(config => config.GetService<IOptionsSnapshot<ConnectionStrings>>().Value);
            builder
                .Register(cfg => cfg.Resolve<IOptionsSnapshot<ConnectionStrings>>().Value)
                .InstancePerLifetimeScope();
        }
    }
}
