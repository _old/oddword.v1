﻿namespace OddWords.NetApi.Startup.Configuration
{
    public class ConnectionStrings
    {
        public string DefaultConnection { get; set; }
    }

    public class MyValues
    {
        public string DefaultValue { get; set; }
    }
}
