﻿using Microsoft.AspNetCore.Cors.Infrastructure;
using System;

namespace OddWords.NetApi.Startup.Configuration
{
    public static class CorsConfig
    {
        public static Action<CorsOptions> Create()
        {
            return options =>
            {
                options.AddPolicy("AllowAllPolicy", builder =>
                {
                    builder.AllowAnyHeader();
                    builder.AllowAnyMethod();
                    builder.AllowAnyOrigin();
                    builder.AllowCredentials();
                    builder.Build();
                });
                options.DefaultPolicyName = "AllowAllPolicy";
            };
        }
    }
}
