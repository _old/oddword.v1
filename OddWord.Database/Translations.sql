﻿CREATE TABLE [dbo].[Translations]
(
	[Id] UNIQUEIDENTIFIER NOT NULL PRIMARY KEY,
	[Text] NVARCHAR(150) NOT NULL UNIQUE,
	[Transcription] NVARCHAR(150) NULL
)
