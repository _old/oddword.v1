﻿CREATE TABLE [dbo].[EntriesTranslations]
(
	[Id] UNIQUEIDENTIFIER NOT NULL PRIMARY KEY,
	[TranslationId] UNIQUEIDENTIFIER NOT NULL,
	[EntryId] UNIQUEIDENTIFIER NOT NULL,
	CONSTRAINT [FK_TranslationID] FOREIGN KEY ([TranslationId]) REFERENCES [dbo].[Translations]([Id]),
	CONSTRAINT [FK_EntryID] FOREIGN KEY ([EntryId]) REFERENCES [dbo].[Entries]([Id])
)
