﻿CREATE TABLE [dbo].[Users]
(
	[Id] UNIQUEIDENTIFIER NOT NULL PRIMARY KEY,
	[Username] NVARCHAR(50) NOT NULL UNIQUE,
	[Passhash] NVARCHAR(128) NOT NULL,
	[Email]    NVARCHAR(252) NOT NULL UNIQUE,
	[Createdon] DATETIME2(2) NOT NULL
)
